const myButton = document.getElementById("myButton");
const continentSpan = document.getElementById("continent");
const countrySpan = document.getElementById("country");
const regionSpan = document.getElementById("region");
const citySpan = document.getElementById("city");
const districtSpan = document.getElementById("district");

myButton.addEventListener("click", findByIp);

async function findByIp() {
    try {
        const response1 = await fetch("https://api.ipify.org/?format=json");
        const ipData = await response1.json();

        const response2 = await fetch(`https://ip-api.com/json/${ipData.ip}`);
        const data2 = await response2.json();

        continentSpan.textContent = data2.continent;
        countrySpan.textContent = data2.country;
        regionSpan.textContent = data2.regionName;
        citySpan.textContent = data2.city;
        districtSpan.textContent = data2.district;
    } catch (error) {
        console.error(error);
    }
}