Asynchronous programming in in JavaScript allows your code to run in the
background without blocking the execution of other code. This means your program not waiting for
every function return, because you declared some functions to be async/await